# ARM kube-state-metrics ChangeLog

## Version 1.2.0 (07/10/2019)

- Add release 1.6.0

## Version 1.1.1 (02/26/2019)

- Fix: ports of the 1.3.0 version

## Version 1.1.0 (02/24/2019)

- Add release 1.5.0

## Version 1.0.0 (02/24/2019)

- Use version 1.4.0
- Migrate to Balenalib Alpine as the Docker base images
- Multi arch support (arm and arm64)
- Add version 1.3.x

## Version 0.2.0 (10/20/2017)

- Add version 1.1.0

## Version 0.1.0 (10/02/2017)

- Add kube-state-metrics 1.0.1
- Use GitlabCI for continuous integration
- Based on Resin Raspbian
