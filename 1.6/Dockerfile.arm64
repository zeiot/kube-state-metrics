# Copyright (C) 2016-2019 Nicolas Lamirault <nicolas.lamirault@gmail.com>

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# VERSION 1.6.0
# AUTHOR:         Nicolas Lamirault <nicolas.lamirault@gmail.com>
# DESCRIPTION:    zeiot/kube-state-metrics

FROM balenalib/aarch64-alpine:3.9
# FROM balenalib/aarch64-debian:stretch
# FROM resin/aarch64-debian:stretch

LABEL summary="KubeStateMetrics for ARM64" \
      description="KubeStateMetrics for ARM64 devices" \
      name="zeiot/kube-state-metrics" \
      url="https://github.com/zeiot/kube-state-metrics" \
      maintainer="Nicolas Lamirault <nicolas.lamirault@gmail.com>"

ENV KUBE_STATE_METRICS_VERSION 1.6.0

RUN [ "cross-build-start" ]

ADD kube-state-metrics-arm64 /usr/bin/kube-state-metrics

RUN adduser -D kube-state-metrics

RUN [ "cross-build-end" ]

USER kube-state-metrics

EXPOSE      8080 8081

ENTRYPOINT  [ "/usr/bin/kube-state-metrics", "--port=8080", "--telemetry-port=8081" ]
